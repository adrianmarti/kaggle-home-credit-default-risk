
import pandas as pd
from contextlib import contextmanager
import time
import luigi
import os


@contextmanager
def timer(title):
    t0 = time.time()
    yield
    print("{} - done in {:.0f}s".format(title, time.time() - t0))


def get_train(features='selected_features.h5', label='TARGET'):
    frame = pd.read_hdf("../data/processed/" + features)
    return frame[frame[label].notnull()]


def get_test(features='selected_features.h5', label='TARGET'):
    frame = pd.read_hdf("../data/processed/" + features)
    return frame[frame[label].isnull()]

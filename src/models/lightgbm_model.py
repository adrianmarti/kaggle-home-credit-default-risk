
import warnings
import collections
from operator import attrgetter
import json
import time
import os
import subprocess
import gc

import numpy as np
import pandas as pd
import lightgbm as lgb
from sklearn import model_selection

from lightgbm import callback
from lightgbm import Booster, Dataset
from lightgbm.compat import integer_types, range_, string_type
import mlflow
import luigi

from features import simple_features


# Parameters from Tilii kernel: https://www.kaggle.com/tilii7/olivier-lightgbm-parameters-by-bayesian-opt/code
param = {'nthread': 4,
         'n_estimators': 10000,
         'learning_rate': 0.02,
         'num_leaves': 34,
         'colsample_bytree': 0.9497036,
         'subsample': 0.8715623,
         'max_depth': 8,
         'reg_alpha': 0.041545473,
         'reg_lambda': 0.0735294,
         'min_split_gain': 0.0222415,
         'min_child_weight': 39.3259775,
         'early_stopping_round': 100}


def flatten_dict(d):
    """Converts a dictionary with 2 layers of keys to one with one layer by
    concatenating key strings."""
    return {"{}_{}".format(k1, k2): d[k1][k2] for k1 in d for k2 in d[k1]}


def record_mlflow(n_fold):
    """Create a callback that records the evaluation history for mlflow."""

    def callback(env):
        """internal function"""
        for data_name, eval_name, result, _ in env.evaluation_result_list:
            mlflow.log_metric("{}_{}_fold{}".format(data_name, eval_name, n_fold), result)
    callback.order = 20
    return callback


def cv(param, frame, num_folds=5, stratified=False, debug=False, label='TARGET', random_state=1001):
    # Cross validation model
    if stratified:
        folds = model_selection.StratifiedKFold(n_splits=num_folds, shuffle=True, random_state=random_state)
    else:
        folds = model_selection.KFold(n_splits=num_folds, shuffle=True, random_state=random_state)

    boosters = []
    for n_fold, (train_idx, valid_idx) in enumerate(folds.split(frame, frame[label])):
        train_x, train_y = frame.iloc[train_idx].drop(label, axis=1), frame[label].iloc[train_idx]
        valid_x, valid_y = frame.iloc[valid_idx].drop(label, axis=1), frame[label].iloc[valid_idx]

        # Using Sklearn API because it gives me a performance boost. Probably because it
        # correctly preprocesses the dataframe.
        cls = lgb.LGBMClassifier(**param)
        cls.fit(train_x, train_y, eval_set=[(train_x, train_y), (valid_x, valid_y)],
                verbose=100, callbacks=[record_mlflow(n_fold)], eval_metric='auc')

        del train_x, train_y, valid_x, valid_y
        gc.collect()
        boosters.append(cls.booster_)

    return boosters


def save_models_cv(boosters, label):
    for i, booster in enumerate(boosters):
        booster.save_model('../models/{}/model_fold_{}'.format(label, i))


def get_last_commit():
    # passing subprocess.PIPE makes run capture stdout, encoding is passed so it
    # doesn't give a byte array that can't be serialized later
    completed_process = subprocess.run(["git", "rev-parse", "HEAD"], stdout=subprocess.PIPE, encoding='utf-8')
    return completed_process.stdout[:-1]


def get_best_iterations(boosters):
    flat_dicts = [flatten_dict(booster.best_score) for booster in boosters]
    by_key = {key: [flat_dict[key] for flat_dict in flat_dicts] for key in flat_dicts[0]}
    means = {"{}_mean".format(key): np.mean(by_key[key]) for key in by_key}
    by_key.update(means)
    return by_key


# cv but wile logging to mlflow
def cv_log(frame, label, params=param, random_state=1001, niter=None):
    """If user provides niter, it overwrites the value provided with the parameters.
    Intended for debugging purposes"""
    iterations_aliases = ['num_iteration', 'n_iter', 'num_tree', 'num_trees', 'num_round', 'num_rounds', 'num_boost_round', 'n_estimators']
    if niter:
        params = {k: niter if k in iterations_aliases else params[k] for k in params}

    path = '../models/{}/'.format(label)

    cv_start_time = time.time()
    boosters = cv(params, frame, random_state=random_state)
    cv_time = time.time() - cv_start_time

    os.mkdir(path)
    save_models_cv(boosters, label)

    mlflow.log_metric('commit_hash', get_last_commit())
    mlflow.log_metric('time', cv_time)

    best_iteration_metrics = get_best_iterations(boosters)
    for metric_key in best_iteration_metrics:
        mlflow.log_metric(metric_key, best_iteration_metrics[metric_key])

    mlflow.log_param('random_state', random_state)
    for param in params:
        mlflow.log_param(param, params[param])


class CV(luigi.Task):
    """Parameters:
        fertureset: Task that yields hdf5 featureset"""
    niter = luigi.Parameter(default=None)
    featureset = luigi.TaskParameter(default=simple_features.GenFeatures)
    label = luigi.Parameter(default='test')
    params = luigi.DictParameter(default=param)
    random_state = luigi.IntParameter(default=1001)
    
    def requires(self):
        return self.featureset().requires()
    
    def run(self):
        features = simple_features.load_features(None)
        features = features[features.TARGET.notnull()]
        cv_log(features, self.label, params=self.params, random_state=self.random_state, niter=self.niter)


# Now some utility functions for results analysis
def load_models_cv(label):
    model_path = '../models/{}/'.format(label)
    fold_paths = [(int(filename[-1]), model_path + filename) for filename in os.listdir(model_path) if filename[:-1] == 'model_fold_']

    # The list should have the boosters in the order as the original list.
    # Note that .sort() on tuples defines the order as the order on the first element
    fold_paths.sort()
    return [lgb.Booster(model_file=path) for _, path in fold_paths]


def get_feature_importances_cv(boosters):
    names = boosters[0].feature_name()
    split_importances = [("fold_{}_split".format(i), booster.feature_importance(importance_type='split')) for i, booster in enumerate(boosters)]
    gain_importances = [("fold_{}_gain".format(i), booster.feature_importance(importance_type='gain')) for i, booster in enumerate(boosters)]

    return pd.DataFrame(dict(split_importances + gain_importances), names)


def make_submission(label, features='selected_features.h5'):
    boosters = load_models_cv(label)
    ids = pd.read_hdf("../data/interm/binary_data.h5", key='application_test').SK_ID_CURR
    features = pd.read_hdf('../data/processed/' + features)
    features = features[features.TARGET.isnull()].drop('TARGET', axis=1)
    dataset = lgb.Dataset(data=features)

    sub_preds = np.zeros(len(ids))  # for some reason lgbm predicts negative values up to -0.068, but kaggle doesn't accept that
    for booster in boosters:
        sub_preds = sub_preds + booster.predict(features) / len(boosters)

    submission = pd.DataFrame({'SK_ID_CURR': ids, 'TARGET': sub_preds})
    submission.to_csv("../submissions/{}.csv".format(label), index=False)

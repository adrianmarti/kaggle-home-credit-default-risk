# home-credit-default-risk

A kaggle competition where I focus on workflow.

## Setup

This project uses the [*luigi*](https://github.com/spotify/luigi) workflow manager. All *luigi* commands must be run inside the `src/` folder and inside a *conda environment*(or equivalent) for it to pick up the correct python, pip, etc executables.

For *Luigi* to work you want to [download your API credentials and put them into ~/.kaggle](https://github.com/Kaggle/kaggle-api#Api), or alternatively manually place the kaggle data into `data/raw`.

## Workflow

I am currently trying to optimize my workflow, so the following text may change across commits.

### The problem I am trying to solve

In the little time I have spend trying to do data science, I observed that I write a single notebook that keeps getting larger and larger. This style of writing has a variety of consequences:

1. **Easy & fast to embed new ideas** into any part of the pipeline, which causes:
2. Some parts are integral to the data pipeline and some parts are just experiments, such that the main pipeline becomes bloated

3. One can make dynamic optimizations to **avoid running compute heavy pieces of code often**, though keep in mind that:
4. If you mess up, you must run the whole notebook again.

5. With due care one can write a notebook linearly, such that the results are easily **reproducible**, but this brings us to:
6. It is hard to write different versions of data pipelines in one notebook, if one wants to have reproducibility

Our goal is to preserve 1., 3. and 5., but removing the other properties.

### Solving the problem(s)

Issue 4. has an obvious solution. Just **write intermediate results to disk**! But this comes with a cost:

7. One has to mentally keep track of which cells one has to run

But this fix actually enables us to to split up the notebook into multiple notebooks! This fixes 7. for obvious reasons and it fixes 6., since one then can write multiple alternative notebooks for a single component of the workflow.

But actually, we just transformed 7. into:

8. One must keep track of the dependencies between notebooks, which impedes reproducibility

As a recap, we have solved 4. and 6., and we still have issues 2. and 8. To tackle 8., one can pass this burden to a **build system** like *make*, or a **workflow manager** like *luigi*. This will ensure that if I require a specific file, I know exactly how it is generated. But to make dependencies clear for notebooks that build on top of other files, it is still not clear what dependencies they have. This can be solved by adding the following lines to the top of the notebook.

```python
from subprocess import run
run(['make', 'required_file_1.h5', 'required_file_2.csv'])
```

or the equivalent in luigi.

### Tradeoff between 1. and 2.

There seems to be a tradeoff between *interactivity* and *preventing bloat*. What I mean by this, is that recording all exploration that you do in jupyter notebooks inherently causes the notebook to become bloated. Conversely, a way you would go about preventing bloat is by isolating the main pipeline from the exploration, but then you severely limit your ability to interact with your pipeline from the exploration kernel. How does this play out on real kernels?

You see, for certain kernels issue 2. is nonexistent, for example for `results.ipynb` or for `loading_speeds.ipynb` kernel. These kernels are what I would call *purely experimental* kernels, since they just have the function to **present** some results and **never had the intention to transform** the data. Thus they don't mix pipeline and experiments.

Issue 2. is also not a problem for the `catboost.ipynb` kernel. It is defining a pipeline, that I ended up not using, thus not running the danger of becoming bloated.

This motivates a mitigation for issue 2., which I will call the *stabilization approach*. With this approach, one would proceed as follows: Start quickly trying out ideas in jupyter notebooks. If they end up being successful I can extract the relevant code and write it into functions or as a script that distills the main ideas(without any bloat). Then I can keep the original notebook with the experiments and for trying new ideas. Also I could tightly interface script and notebook by reusing functions in the script inside the notebook. The autoreload feature of jupyter notebooks is really a blessing for doing this.

### Reproducibility vs explaining how one got to the final model

Now that reproducibility is not a problem anymore, we can start tackling the problem of documenting the way a model was created. For this I propose 2 things:

1) Use git to keep track of the whole code

2) Logging every model that is evaluated and the exact commit hash so in the future one can go back in time and look at the differences

### Create one feature generator script for each dataset?

You see, the feature generating code takes **minutes** to run. And before you ask, yes it is the feature generating code and not the data loading, which takes 1.7s for the biggest feature batch, which is nothing compared to the feature engineering code. I thought it would be a good idea to dedicate one script to each dataset and then execute it with make, though in the end I think jupyter notebooks will do. These were my thoughts when I was still using make, where adding so many feature generating scripts would be a hastle, but with *luigi*, it is barely even more complex than generating all at once.

### Sklearn pipelines

These creatures also have their place, while one should be clear on that they do not replace a workflow manager, but instead can be written inside *luigi* tasks. For example I can write components like one-hot encoders or PCA-categorical encoders or NN-encoders as `sklearn` estimators and just concatenate them cleanly. It is to be clear whether this is better than just plain old functions and composition. Maybe even meta-stuff like finding the best encoder is easier.


## Design choices

### Conversion to binary format

Performing this step on the raw data causes a 6x loading speed increase, see `loading_speeds.ipynb` kernel. It's totally worth it, since that data is going to be loaded countless times.

### Experiment tracking

* **comet.ml** seems very promising, it is as easy as one could imagine to track experiments. The catch is that it *only* works with usuing the *comet.ml* server(which is not open source), so I don't actually have access to my logs unless I use the REST api. It just does what I need, experiment logging and nice visualization.
* **sacret**  makes use of fancy function annotations to track experiments and config. To me it seems more framework-like than library-like, which is a minus. It can write its logs to a *MongoDB*, which can then be visualized with open source external tools.
* **MLFlow** provides a lot of unrelated features, many more than *sacret* and *comet.ml*. It also has a framework caracter to it, but it really leverages its position as one. It has:
    * a way to manage project dependencies
    * a way unified to store models and run them in the cloud(Only azure and aws)
    * git commit, metrics, parameters,... tracking(what I actually wanted)

 If I use *sacret* or *comet.ml*, I will need to somehow insert my performance into the git commit and the commit hash into the experiment entry.


## Project Organization

    ├── LICENSE
    ├── Makefile           <- Makefile with commands that perform parts of the processing pipeline
    ├── README.md          <- The top-level README
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interm         <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump. Folder gets generated by luigi.
    |
    ├── input              <- Symlink to data/raw. Enables compatibility with existing kernels, not
    |                         sure if I will really use this. But DON'T remove this with `rm -rf input/`,
    |                         because this will delete the contents of `data/raw/`, too. Remove it with
    |                         `rm input`.
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    |
    ├── submissions        <- Directory to keep submissions
    │
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions for submissions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │

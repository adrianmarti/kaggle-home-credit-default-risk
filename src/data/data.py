
import luigi
import subprocess
import os
import pandas as pd
import utils
import kaggle


def mem_usage(pandas_obj):
    if isinstance(pandas_obj,pd.DataFrame):
        usage_b = pandas_obj.memory_usage(deep=True).sum()
    else:  # we assume if not a df it's a series
        usage_b = pandas_obj.memory_usage(deep=True)
    usage_mb = usage_b / 1024 ** 2  # convert bytes to megabytes
    return "{:03.2f} MB".format(usage_mb)


def compress_dataset(df, categorize_objects=False):
    types = df.dtypes
    int_features = types[types == 'int'].keys()
    float_features = types[types == 'float'].keys()
    cat_features = types[types == 'object'].keys()

    print('memory usage before compression: ', mem_usage(df))
    df[int_features] = df[int_features].apply(pd.to_numeric, downcast='unsigned')
    print('memory usage after casting ints: ', mem_usage(df))

    df[float_features] = df[float_features].apply(pd.to_numeric, downcast='float')
    print('memory usage after casting floats: ', mem_usage(df))

    if len(cat_features) != 0 and categorize_objects:
        df[cat_features] = df[cat_features].astype('category')
        print('memory usage after categorizing objects: ', mem_usage(df))

    return df


def is_dataset(filename):
    return (filename != 'HomeCredit_columns_description.csv') and (filename != 'sample_submission.csv.zip')


class DownloadRaw(luigi.Task):
    def output(self):
        return luigi.LocalTarget("../data/raw/")

    def run(self):
        with self.output().temporary_path() as temp_path:
            subprocess.run(["kaggle", "competitions", "download", "home-credit-default-risk", "-p", temp_path])


# took 83 seconds to execute
class CompressRaw(luigi.Task):
    nrows = luigi.IntParameter(default=None)

    def requires(self):
        return DownloadRaw()
    
    def output(self):
        debug = self.nrows is not None
        return luigi.LocalTarget("../data/interm/compressed_data{}.h5".format('_debug' if debug else ''))

    def run(self):
        filenames = [filename for filename in os.listdir(self.input().path) if is_dataset(filename)]
        with utils.timer("Compressing the raw features"):
            with self.output().temporary_path() as temp_path:
                for filename in filenames:
                    print('compressing ', filename)
                    df = pd.read_csv(self.input().path + filename, nrows=self.nrows)
                    df = compress_dataset(df)

                    without_extension = filename.split('.')[0]
                    df.to_hdf(temp_path, key=without_extension)


# TODO: add data imputation Task